﻿#include "stdafx.h"

#pragma region funkcja czy_pierwsza_pierwsza_wersja
bool czy_pierwsza_I_wersja(unsigned long long int n)
{
	if(n<2)
		return false; 

	for(unsigned long long int i=2;i<n;i++)
		if(n%i==0)
			return false; 
	return true;
}  
#pragma endregion

#pragma region funkcja czy_pierwsza_II_wersja
bool czy_pierwsza_II_wersja(unsigned long long int n)
{
	if(n<2)
		return false; //gdy liczba jest mniejsza niż 2 to nie jest pierwszą
	//	long double pierwiastek = sqrt(n);
	// for(unsigned long long int i=2;i<=pierwiastek;i++)
	for(unsigned long long int i=2;i<=sqrt(n);i++)
		if(n%i==0)
			return false; 
	return true;
}    
#pragma endregion

#pragma region sito

unsigned long long int trzecia_metoda(unsigned long long int od, unsigned long long int n)
{
	unsigned long i, count = 0;
	unsigned long long int *a = new unsigned long long int[n];


	for (i = od; i < n; i++) a[i] = true;

	for (i = od; i < n; i++)
		if (a[i])
			for (unsigned long long int j = i; j*i < n; j++) a[i*j] = false;

	for (i = od; i < n; i++)
		if (a[i])
		{
			count++;
			// cout << i << " ";
		}
		delete [] a;
		return count;
}
#pragma endregion

#pragma region sito_lista
int sito_lista(int od, int l)
{
    int licznik = 0;
    int i,k,m,n=od;
    list<int> lista;
    for(i=1;i<l;i++)
    {
        lista.push_back(i+1);
    }
	double pier = sqrt(l);
    for( k=od; k <= pier; k++ )
    {
        for( n=k*k ; n<=l ; n+=k ) lista.remove(n);
    }
    list<int>::iterator iter;
    for(iter=lista.begin(); iter != lista.end(); iter++)
    {
 //       cout << *iter << " ";
        licznik++;
    }
    return licznik;
}
#pragma endregion

#pragma region sprawdzacz
// co by sobie sprawdzić inne metody
void sprawdzacz(unsigned long long int x, unsigned long long int y)
{
	unsigned long long int  i,ile = 0;
	unsigned long long int wart;



	if (x == y)
	{
		if (x == 1)  ++ile;
	}
	else
	{ 
		if (x == 1)  ++ile;
		if (y == 1)  ++ile;
	}   

	for(i = x; i <=y ;i++)
	{
		wart=0;          
		for (unsigned long long int j = 2; j<=i;++j)   
			if ((i % j) == 0) ++wart;
		if (wart == 1 )
			++ile;  
	}


	cout << " przedzial x y <" << x << "," << y << "> zawiera " << ile << " liczb pierwszych." << endl;
}
#pragma endregion

#pragma region pierwsza_metoda
unsigned long long int pierwsza_metoda(unsigned long long int od, unsigned long long int zakres)
{
	unsigned long long int licznik = 0;
	unsigned long long int delta;
	for (unsigned long long int liczba = od; liczba<=zakres;liczba++)
	{
		if (czy_pierwsza_I_wersja(liczba)) licznik++;
	}
	return licznik;
}  
#pragma endregion

#pragma region druga_metoda
unsigned long long int druga_metoda(unsigned long long int od, unsigned long long int zakres)
{
	unsigned long long int licznik = 0;
	unsigned long long int delta;
	for (unsigned long long int liczba = od; liczba<=zakres;liczba++)
	{
		if (czy_pierwsza_II_wersja(liczba)) licznik++;
	}
	return licznik;
}  
#pragma endregion

#pragma region cls()
void cls()
{
	HANDLE hStdout;
	hStdout = GetStdHandle( STD_OUTPUT_HANDLE );
	COORD coordScreen = { 0, 0 };
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	if( !GetConsoleScreenBufferInfo( hStdout, & csbi ) )
		return;

	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;
	if( !FillConsoleOutputCharacter( hStdout,( TCHAR ) ' ',
		dwConSize, coordScreen, & cCharsWritten ) )
		return;

	if( !GetConsoleScreenBufferInfo( hStdout, & csbi ) )
		return;

	if( !FillConsoleOutputAttribute( hStdout, csbi.wAttributes,
		dwConSize, coordScreen, & cCharsWritten ) )
		return;

	SetConsoleCursorPosition( hStdout, coordScreen );
}
#pragma endregion


int _tmain(int argc, _TCHAR* argv[])
{

#pragma region deklaracje
	fstream plik;
	clock_t start, koniec;
	unsigned long long int ile,czas, gorna_granica,dolna_granica,interwal, ktora_metoda;
	string nazwa_pliku_z_koncowka;
	char znak, kolo;  
#pragma endregion



#pragma region do while program
	do
	{
#pragma region pogadajmy
		cout << "Podaj przedzial w formacie (a,b) i daj enter \n";
		cout << "a wieksze rowne 2, b mniejsze rowne 18 446 744 073 709 551 615, poprosze zakres: \n";
		cin>>znak;cin >> dolna_granica;cin >> znak;cin>>gorna_granica;cin>>znak;
		cout << "Czy liczymy po calym zakresie (podaj c) czy przedzialowo (podaj p) "; cin >> znak;
		if (znak != 'c' && znak != 'C')
		{
			cout << "Teraz podaj interwal "; cin >> interwal;
		}
		else
		{
			interwal = gorna_granica-dolna_granica;
		}
		cout << "Ktorymi metodami liczymy wybierz odpowiednio: \n"
			"1 - pierwsza metoda\n"
			"2 - druga metoda\n"
			"3 - trzecia metoda\n"
			"4 - dodatkowo sito na liscie\n"
			"34 - dwa sita\n"
			"123 - trzy metody opisane w projekcie\n"
			"1234 - wszystkimi trzema metodami\n Poprosze o odp: ";
		cin >> ktora_metoda;	
		cout << "Teraz powiedz czy chcesz zapisac wyniki do pliku, wybierz t/n: "; cin >> znak;

		if (znak == 't' || znak == 'T')
		{
			cout << "Podaj nazwe pliku z koncowka txt (tam poleca wyniki)"; cin >> nazwa_pliku_z_koncowka;
			plik.open(nazwa_pliku_z_koncowka,ios::out | ios::app); 
		}
		else
			cout << "\nNie to nie.\n";  
#pragma endregion

#pragma region petla liczaca
		for (unsigned long long int zak = dolna_granica; zak <= gorna_granica; zak+=interwal)

		{


			if (zak > dolna_granica)
			{
				cout << "######################################################\n";
				cout << "######################################################\n";
				cout << "\nPoliczymy teraz w zakresie (" << dolna_granica << "," << zak << ")" << endl; 
			}


#pragma region sprawdzamy pierwsza metoda
			if (zak > dolna_granica && (ktora_metoda == 1 || ktora_metoda == 1234 || ktora_metoda == 123 ))
			{
				start = clock();
				ile = pierwsza_metoda(dolna_granica,zak);
				koniec = clock();
				czas = unsigned long long int(koniec - start);
				cout << "Pierwsza metoda w zakresie (" << dolna_granica << "," << zak << ") mamy "<< ile << " liczb pierwszych.\n"
					"To skomplikowane oblczenie zdodnie z pierwszym algorytmem zajelo " << czas << " ms.\n\n";
				if (znak == 't' || znak == 'T')
				{
					plik << "Metoda 1: (" << dolna_granica << "," << zak << ")  Czas = " << czas << " ms." << endl; 
					//	plik << czas << endl;
				}
			}  
#pragma endregion


#pragma region sprawdzamy druga metoda
			if (zak > dolna_granica && (ktora_metoda == 2 || ktora_metoda == 1234 || ktora_metoda == 123))
			{
				start = clock();
				ile = druga_metoda(dolna_granica, zak);
				koniec = clock();
				czas = unsigned long long int(koniec - start);
				cout << "Druga metoda w zakresie (" << dolna_granica << "," << zak << ") mamy "<< ile << " liczb pierwszych.\n"
					"To skomplikowane oblczenie zdodnie z drugim algorytmem zajelo " << czas << " ms.\n\n";
				if (znak == 't' || znak == 'T')
				{
					plik << "Metoda 2: ("<< dolna_granica << "," << zak << ")  Czas = " << czas << " ms." << endl;
					//plik << czas << endl;
				}
			}  
#pragma endregion


#pragma region sprawdzamy trzecia metoda
			if (zak > dolna_granica && (ktora_metoda == 3 || ktora_metoda == 1234 || ktora_metoda == 34 || ktora_metoda == 123 ))
			{
				start = clock();
				ile = trzecia_metoda(dolna_granica,zak);
				koniec = clock();
				czas = unsigned long long int(koniec - start);
				cout << "Trzecia metoda w zakresie (" << dolna_granica << "," << zak << ") mamy "<< ile << " liczb pierwszych.\n"
					"To skomplikowane oblczenie zdodnie z trzecim algorytmem zajelo " << czas << " ms.\n\n";
				if (znak == 't' || znak == 'T')
				{
					plik << "Metoda 3: (0" << dolna_granica << "," << zak << ")  Czas = " << czas << " ms." << endl;
					//plik << czas << endl;
				}
			}  
#pragma endregion


#pragma region sprawdzamy trzecia metoda - lista
			if (zak > dolna_granica && (ktora_metoda == 4 || ktora_metoda == 34 || ktora_metoda==1234  ))
			{
				start = clock();
				ile = sito_lista (dolna_granica,zak);
				koniec = clock();
				czas = unsigned long long int(koniec - start);
				cout << "Sito na liscie w zakresie (" << dolna_granica << "," << zak << ") mamy "<< ile << " liczb pierwszych.\n"
					"To skomplikowane oblczenie zdodnie z trzecim algorytmem zajelo " << czas << " ms.\n\n";
				if (znak == 't' || znak == 'T')
				{
					plik << "Metoda 3 (lista): (0" << dolna_granica << "," << zak << ")  Czas = " << czas << " ms." << endl;
					//plik << czas << endl;
				}
			}  
#pragma endregion

		}

		if (znak == 't' || znak == 'T')

		{
			plik.close(); 
		}  
#pragma endregion 

		cout << "Czy chcesz kontynuowac t/n?: "; cin >> kolo;
		cls();
	} while (kolo == 't' || kolo == 'T');  
#pragma endregion


	return 0;
}